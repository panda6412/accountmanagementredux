import React, { Component } from "react";
import "./App.css";
import SearchBarComponent from "./components/SearchBar";
import ResultTableComponent from "./components/ResultTable";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SearchBarComponent />
        <ResultTableComponent />
      </div>
    );
  }
}

export default App;
