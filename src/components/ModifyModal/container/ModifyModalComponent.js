import React, { Component } from "react";
import { Modal, Button, Header, Form, Message } from "semantic-ui-react";
import { bindActionCreators } from "redux";
import * as defaultActions from "../../../actions";
import { connect } from "react-redux";
import * as currencyServices from "../../../services/currenyService";

class ModifyModalComponent extends Component {
  state = {
    curOptions: [],
    open: false
  };

  componentDidMount = () => {
    this.initCurOptions();
  };

  initCurOptions = async () => {
    let curMap = await currencyServices.getAllCurrencies();
    let options = [];
    Object.entries(curMap).forEach(entry => {
      const text = `${entry[0]} - ${entry[1]}`;
      options.push({ key: entry[0], text: text, value: entry[0] });
    });
    this.setState({ curOptions: options });
  };

  confirmButtonClick = () => {
    const { state, actions, onModify } = this.props;
    const { modifyData } = state;
    onModify(modifyData);
    actions.retrieveAccounts();
    this.closeModal();
  };

  cancelButtonClick = () => {
    this.closeModal();
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { state, actions, doOpen } = this.props;
    const { modifyData } = state;
    const { open, curOptions } = this.state;
    return (
      <Modal
        open={open}
        onClose={() => this.closeModal()}
        trigger={
          <Button
            icon="plus"
            content="修改"
            onClick={() => {
              doOpen(() => this.setState({ open: true }));
            }}
          />
        }
        closeIcon
      >
        <Header icon="info circle" content="Modify new account" />

        <Modal.Content>
          <Form loading={false} widths="equal" error={!!state.errorHint}>
            <Form.Group>
              <Form.Select
                fluid
                label="幣別"
                name="currencyName"
                options={curOptions || []}
                placeholder="幣別"
                value={modifyData.currencyName || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="帳號名稱"
                name="acntName"
                plcaeholder="帳號名稱"
                value={modifyData.acntName || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="帳號狀態"
                name="acntStatusName"
                plcaeholder="帳號狀態"
                value={modifyData.acntStatusName || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Input
                label="帳號性質"
                name="acntTypeName"
                plcaeholder="帳號性質"
                value={modifyData.acntTypeName || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="銀行帳號"
                name="bankAccount"
                plcaeholder="銀行帳號"
                value={modifyData.bankAccount || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="國家"
                name="countryName"
                plcaeholder="國家"
                value={modifyData.countryName || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Input
                label="銀行"
                name="bankName"
                plcaeholder="銀行"
                value={modifyData.bankName || ""}
                onChange={(e, ele) =>
                  actions.modifyDataChange(ele.name, ele.value)
                }
              />
            </Form.Group>
            <Message error header="修改失敗" content={state.errorHint} />
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            color="green"
            basic
            content="Confirm"
            onClick={() => this.confirmButtonClick()}
          />
          <Button
            color="red"
            content="cancel"
            basic
            onClick={() => this.cancelButtonClick()}
          />
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({ state });
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(defaultActions, dispatch)
});

ModifyModalComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModifyModalComponent);

export default ModifyModalComponent;
