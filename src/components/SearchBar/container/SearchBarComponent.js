import React, { Component } from "react";
import { Form, Button, Segment } from "semantic-ui-react";
import * as currencyServices from "../../../services/currenyService";
import * as defaultActinos from "../../../actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import AddModalComponent from "../../AddModal/container/AddModalComponent";

const curOptions = [];
const initCurOptions = async () => {
  let curMap = await currencyServices.getAllCurrencies();
  Object.entries(curMap).forEach(entry => {
    const text = `${entry[0]} - ${entry[1]}`;
    curOptions.push({ key: entry[0], text: text, value: entry[0] });
  });
};

class SearchBarComponent extends Component {
  componentDidMount = () => {
    console.log("component did mount");
    initCurOptions();
  };

  render() {
    console.log("search bar has been rendered !");
    const { state, actions } = this.props;
    return (
      <Segment>
        <Form loading={false} widths="equal">
          <Form.Group>
            <Form.Select
              fluid
              label="幣別"
              name="currencyName"
              options={curOptions}
              placeholder="幣別"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
            <Form.Input
              label="帳號名稱"
              name="acntName"
              plcaeholder="帳號名稱"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
            <Form.Input
              label="帳號狀態"
              name="acntStatusName"
              plcaeholder="帳號狀態"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Input
              label="帳號性質"
              name="acntTypeName"
              plcaeholder="帳號性質"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
            <Form.Input
              label="銀行帳號"
              name="bankAccount"
              plcaeholder="銀行帳號"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
            <Form.Input
              label="國家"
              name="countryName"
              plcaeholder="國家"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Input
              label="銀行"
              name="bankName"
              plcaeholder="銀行"
              onChange={(e, ele) => {
                actions.conditionDataChange(ele.name, ele.value);
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Field style={{ textAlign: "right" }}>
              <AddModalComponent />
              <Button
                icon="search"
                content="查詢"
                onClick={() => actions.retrieveAccounts(state.conditions)}
              />
            </Form.Field>
          </Form.Group>
        </Form>
      </Segment>
    );
  }
}

const mapStateToProps = state => ({ state });

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(defaultActinos, dispatch)
});

SearchBarComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBarComponent);

export default SearchBarComponent;
