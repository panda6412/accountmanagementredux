import React, { Component } from "react";
import { Modal, Header, Button } from "semantic-ui-react";
import { bindActionCreators } from "redux";
import * as defaultActions from "../../../actions";
import { connect } from "react-redux";

class DeleteModalComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  confirmButtonClick = () => {
    const { onDelete, actions } = this.props;
    onDelete();
    actions.retrieveAccounts();
    this.closeModal();
  };

  cancelButtonClick = () => {
    this.closeModal();
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { doOpen } = this.props;
    return (
      <Modal
        trigger={
          <Button
            content="刪除"
            onClick={() => {
              doOpen(() => this.setState({ open: true }));
            }}
          />
        }
        open={this.state.open}
      >
        <Header icon="info circle" content="修改帳戶資料" />
        <Modal.Content>
          <p>確認刪除此筆資料？</p>
        </Modal.Content>
        <Modal.Actions>
          <Button
            color="green"
            basic
            content="Confirm"
            onClick={() => {
              this.confirmButtonClick();
            }}
          />
          <Button
            color="red"
            content="cancel"
            basic
            onClick={() => this.cancelButtonClick()}
          />
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({ state });
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(defaultActions, dispatch)
});

DeleteModalComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteModalComponent);

export default DeleteModalComponent;
