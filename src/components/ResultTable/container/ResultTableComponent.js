import React, { Component } from "react";
import { Table } from "semantic-ui-react";
import * as defaultActions from "../../../actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import ModifyModalComponent from "../../ModifyModal";
import DeleteModalComponent from "../../DeleteModal";

class ResultTableComponent extends Component {
  componentDidMount() {}

  modifyButtonClick = () => {
    const { actions } = this.props;
    actions.switchModifyModal(true);
  };

  deleteButtonClick = () => {
    const { actions } = this.props;
    actions.switchDeleteModal(true);
  };

  render() {
    const { actions, state } = this.props;
    const { resultList } = state;
    return (
      <div>
        <Table selectable celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>幣別</Table.HeaderCell>
              <Table.HeaderCell>帳號名稱</Table.HeaderCell>
              <Table.HeaderCell>帳號狀態</Table.HeaderCell>
              <Table.HeaderCell>帳號性質</Table.HeaderCell>
              <Table.HeaderCell>帳號銀行</Table.HeaderCell>
              <Table.HeaderCell>國家</Table.HeaderCell>
              <Table.HeaderCell>銀行</Table.HeaderCell>
              <Table.HeaderCell />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {resultList.map((account, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>{account.currencyName}</Table.Cell>
                  <Table.Cell>{account.acntName}</Table.Cell>
                  <Table.Cell>{account.acntStatusName}</Table.Cell>
                  <Table.Cell>{account.acntTypeName}</Table.Cell>
                  <Table.Cell>{account.bankAccount}</Table.Cell>
                  <Table.Cell>{account.countryName}</Table.Cell>
                  <Table.Cell>{account.bankName}</Table.Cell>
                  <Table.Cell style={{ textAlign: "center" }}>
                    <ModifyModalComponent
                      onModify={account => {
                        actions.modifyAccount(account.pk, account);
                      }}
                      doOpen={openFunc => {
                        openFunc();
                        Object.entries(account).forEach(entry => {
                          actions.modifyDataChange(entry[0], entry[1]);
                        });
                      }}
                    />
                    <DeleteModalComponent
                      onDelete={() => {
                        actions.deleteAccount(account.pk);
                      }}
                      doOpen={openFunc => {
                        openFunc();
                      }}
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = state => ({ state });

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(defaultActions, dispatch)
});

ResultTableComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultTableComponent);

export default ResultTableComponent;
