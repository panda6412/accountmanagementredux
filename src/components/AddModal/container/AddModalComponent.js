import React, { Component } from "react";
import { Modal, Header, Button, Form, Message } from "semantic-ui-react";
import { bindActionCreators } from "redux";
import * as defaultActions from "../../../actions";
import { connect } from "react-redux";
import * as currencyServices from "../../../services/currenyService";

const curOptions = [];
const initCurOptions = async () => {
  let curMap = await currencyServices.getAllCurrencies();
  Object.entries(curMap).forEach(entry => {
    const text = `${entry[0]} - ${entry[1]}`;
    curOptions.push({ key: entry[0], text: text, value: entry[0] });
  });
};

class AddModalComponent extends Component {
  componentDidMount = async () => {
    await initCurOptions();
  };

  confirmButtonClick = () => {
    const { state, actions } = this.props;
    const { insertData: account, accounts } = state;

    if (this.isValidAccount(account) && !this.isAccountDulplicated(account)) {
      // Add account ..
      account["pk"] = accounts.length + 1;
      actions.addAccount(account);

      // Refresh table ..
      actions.retrieveAccounts();

      // After complete adding, then close the modal ..
      this.closeModal();
    }
  };

  cancelButtonClick = () => {
    this.closeModal();
  };

  closeModal = () => {
    const { actions } = this.props;

    // Reset fields ..
    this.resetFields();

    // Close the add modal ..
    actions.switchAddModal(false);
  };

  isValidAccount = account => {
    const { actions } = this.props;
    // All field should filled
    let isValid = true;
    if (Object.getOwnPropertyNames(account).length === 0) {
      isValid = false;
    }

    Object.entries(account).forEach(entry => {
      const value = entry[1];
      if (value === undefined || value === "") {
        isValid = false;
      }
    });

    if (!isValid) {
      actions.errorHintChange("無效的帳號資訊");
    }
    return isValid;
  };

  isAccountDulplicated = account => {
    const { state, actions } = this.props;
    const { accounts } = state;

    let filteredAcct = accounts.filter(
      acct => acct.bankAccount === account.bankAccount
    );

    const isDulplicated = filteredAcct.length > 0 ? true : false;
    if (isDulplicated) {
      actions.errorHintChange(`${account.bankAccount} 已經重複了`);
    }

    return isDulplicated;
  };

  resetFields = () => {
    const { state, actions } = this.props;
    const { insertData } = state;
    Object.entries(insertData).forEach(entry => {
      actions.insertDataChange([entry[0]], "");
    });

    // Clear hint message ..
    actions.errorHintChange("");
  };

  render() {
    const { state, actions } = this.props;
    const { insertData } = state;
    return (
      <Modal
        open={state.addModalSwitch}
        onClose={() => this.closeModal()}
        trigger={
          <Button
            icon="plus"
            content="新增"
            onClick={() => actions.switchAddModal(true)}
          />
        }
        closeIcon
      >
        <Header icon="info circle" content="Add new account" />

        <Modal.Content>
          <Form loading={false} widths="equal" error={!!state.errorHint}>
            <Form.Group>
              <Form.Select
                fluid
                label="幣別"
                name="currencyName"
                options={curOptions}
                placeholder="幣別"
                value={insertData.currencyName || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="帳號名稱"
                name="acntName"
                plcaeholder="帳號名稱"
                value={insertData.acntName || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="帳號狀態"
                name="acntStatusName"
                plcaeholder="帳號狀態"
                value={insertData.acntStatusName || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Input
                label="帳號性質"
                name="acntTypeName"
                plcaeholder="帳號性質"
                value={insertData.acntTypeName || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="銀行帳號"
                name="bankAccount"
                plcaeholder="銀行帳號"
                value={insertData.bankAccount || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
              <Form.Input
                label="國家"
                name="countryName"
                plcaeholder="國家"
                value={insertData.countryName || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Input
                label="銀行"
                name="bankName"
                plcaeholder="銀行"
                value={insertData.bankName || ""}
                onChange={(e, ele) =>
                  actions.insertDataChange(ele.name, ele.value)
                }
              />
            </Form.Group>
            <Message error header="新增失敗" content={state.errorHint} />
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button
            color="green"
            basic
            content="Confirm"
            onClick={() => this.confirmButtonClick()}
          />
          <Button
            color="red"
            content="cancel"
            basic
            onClick={() => this.cancelButtonClick()}
          />
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({ state });
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(defaultActions, dispatch)
});

AddModalComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddModalComponent);

export default AddModalComponent;
