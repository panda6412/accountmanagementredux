import { ACTIONS } from "../actions";

var databaseList = [
  {
    pk: 1,
    currencyName: "TWD",
    acntName: "高雄收入",
    acntStatusName: "正常",
    acntTypeName: "活期存款",
    bankAccount: "107540536584",
    countryName: "台灣",
    bankName: "中國信託商業銀"
  },
  {
    pk: 2,
    currencyName: "TWD",
    acntName: "西門零用金戶",
    acntStatusName: "正常",
    acntTypeName: "活期存款",
    bankAccount: "186530018350",
    countryName: "台灣",
    bankName: "中國信託商業銀"
  },
  {
    pk: 3,
    currencyName: "TWD",
    acntName: "中壢收入",
    acntStatusName: "正常",
    acntTypeName: "活期存款",
    bankAccount: "901540540653",
    countryName: "台灣",
    bankName: "中國信託商業銀"
  },
  {
    pk: 4,
    currencyName: "TWD",
    acntName: "新竹收入",
    acntStatusName: "正常",
    acntTypeName: "活期存款",
    bankAccount: "174540582189",
    countryName: "台灣",
    bankName: "中國信託商業銀"
  },
  {
    pk: 5,
    currencyName: "TWD",
    acntName: "新竹零用金戶",
    acntStatusName: "正常",
    acntTypeName: "活期存款",
    bankAccount: "118540446980",
    countryName: "台灣",
    bankName: "中國信託商業銀"
  },
  {
    pk: 6,
    currencyName: "USD",
    acntName: "西門零用金戶",
    acntStatusName: "正常",
    acntTypeName: "活期存款",
    bankAccount: "1234566",
    countryName: "台灣",
    bankName: "中國信託商業銀"
  }
];

const initialState = {
  accounts: databaseList,
  resultList: [],
  conditions: {},
  insertData: {},
  modifyData: {},
  addModalSwitch: false,
  modifyModalSwitch: false,
  deleteModalSwitch: false,
  errorFields: [],
  errorHint: ""
};

let filterDataByConditions = (origin, conditions = []) => {
  let queryResult = [];
  queryResult = origin.filter(account => {
    return isMatchAccountFromConditions(account, conditions);
  });
  return queryResult;
};

let isMatchAccountFromConditions = (account, conditions) => {
  let filter = true;
  for (let key in conditions) {
    if (conditions[key] === "") {
      continue;
    }
    if (account[key] === undefined || account[key] !== conditions[key]) {
      filter = filter && false;
    } else {
      filter = filter && true;
    }
  }
  return filter;
};

function accountManagementReducer(state = initialState, action) {
  let { accounts } = state;
  const { type, flag } = action;
  switch (type) {
    case ACTIONS.CONDITION_DATA_CHANGE:
      return {
        ...state,
        conditions: { ...state.conditions, [action.field]: action.value }
      };

    case ACTIONS.INSERT_DATA_CHANGE:
      return {
        ...state,
        insertData: { ...state.insertData, [action.field]: action.value }
      };

    case ACTIONS.MODIFY_DATA_CHANGE:
      return {
        ...state,
        modifyData: { ...state.modifyData, [action.field]: action.value }
      };

    case ACTIONS.RETRIEVE_ACCOUNTS:
      return {
        ...state,
        resultList: filterDataByConditions(state.accounts, action.conditions)
      };

    case ACTIONS.ADD_ACCOUNT:
      state.accounts.push(action.account);
      return {
        ...state,
        accounts: [...state.accounts]
      };

    case ACTIONS.MODIFY_ACCOUNT:
      let replaceIndex = accounts.findIndex(acct => acct.pk === action.pk);
      accounts.splice(replaceIndex, 1, action.account);
      return {
        ...state,
        accounts: [...accounts]
      };

    case ACTIONS.DELETE_ACCOUNT:
      accounts = accounts.filter(account => account.pk !== action.pk);
      return {
        ...state,
        accounts: [...accounts]
      };

    case ACTIONS.SWITCH_ADD_MODAL:
      return {
        ...state,
        addModalSwitch: flag
      };

    case ACTIONS.SWITCH_MODIFY_MODAL:
      return {
        ...state,
        modifyModalSwitch: flag
      };

    case ACTIONS.SWITCH_DELETE_MODAL:
      return {
        ...state,
        deleteModalSwitch: flag
      };

    case ACTIONS.ERROR_HINT_CHANGE:
      return {
        ...state,
        errorHint: action.errorHint
      };

    case ACTIONS.ERROR_FIELDS_CHANGE:
      return {
        ...state,
        errorFields: [...action.errorFields]
      };

    default:
      return { ...state };
  }
}

export default accountManagementReducer;
