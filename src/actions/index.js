export const ACTIONS = {
  CONDITION_DATA_CHANGE: "CONDITION_DATA_CHANGE",
  INSERT_DATA_CHANGE: "INSERT_DATA_CHANGE",
  MODIFY_DATA_CHANGE: "MODIFY_DATA_CHANGE",
  RETRIEVE_ACCOUNTS: "RETRIEVE_ACCOUNTS",
  ADD_ACCOUNT: "ADD_ACCOUNT",
  DELETE_ACCOUNT: "DELETE_ACCOUNT",
  MODIFY_ACCOUNT: "MODIFY_ACCOUNT",
  SWITCH_ADD_MODAL: "SWITCH_ADD_MODAL",
  SWITCH_MODIFY_MODAL: "SWITCH_MODIFY_MODAL",
  SWITCH_DELETE_MODAL: "SWITCH_DELETE_MODAL",
  ERROR_HINT_CHANGE: "ERROR_HINT_CHANGE",
  ERROR_FIELDS_CHANGE: "ERROR_FIELDS_CHANGE"
};

export const conditionDataChange = (field, value) => ({
  type: "CONDITION_DATA_CHANGE",
  field,
  value
});

export const insertDataChange = (field, value) => ({
  type: "INSERT_DATA_CHANGE",
  field,
  value
});

export const modifyDataChange = (field, value) => ({
  type: "MODIFY_DATA_CHANGE",
  field,
  value
});

export const retrieveAccounts = conditions => ({
  type: "RETRIEVE_ACCOUNTS",
  conditions
});

export const addAccount = account => ({
  type: "ADD_ACCOUNT",
  account
});

export const deleteAccount = pk => {
  return { type: "DELETE_ACCOUNT", pk };
};

export const modifyAccount = (pk, account) => ({
  type: "MODIFY_ACCOUNT",
  pk,
  account
});

export const switchAddModal = flag => ({
  type: "SWITCH_ADD_MODAL",
  flag: flag
});

export const switchModifyModal = flag => ({
  type: "SWITCH_MODIFY_MODAL",
  flag: flag
});

export const switchDeleteModal = flag => ({
  type: "SWITCH_DELETE_MODAL",
  flag: flag
});

export const errorHintChange = errorHint => ({
  type: "ERROR_HINT_CHANGE",
  errorHint: errorHint
});

export const errorFieldsChange = errorFields => ({
  type: "ERROR_FIELDS_CHANGE",
  errorFields: errorFields
});
