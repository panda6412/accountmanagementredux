export const getAllCurrencies = async () => {
  const response = await fetch(
    "https://openexchangerates.org/api/currencies.json"
  );
  const json = await response.json();
  return json;
};
